<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="col-md-19">
	<div id="content">
        	<?php echo $content; ?>
	</div><!-- content -->
</div>

<?php $this->endContent(); ?>