
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />


<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Вход';

?>



<div class="main-box col-sm-4 col-sm-offset-4">
<div class="form-signin">
  <fieldset>
 
		<legend>Вход в личный кабинет</legend>  
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'login-form',
        'type'=>'horizontal',
        'htmlOptions' => array('class' => 'well center'), // for inset effect
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
     
	<p class="note">Поля отмеченные <span class="required">*</span> обязательны к заполнению.</p>

         <?php echo $form->maskedTextFieldGroup($model, 'username', array(
    'widgetOptions'=>array(
         'mask' => '+7 (iii) iii ii ii',
         'charMap' => array('i' => '[0-9]'),
         'htmlOptions'=>array('class'=>'span5','maxlength'=>17)
     ) 
      ));
 ?>
        
	<?php 
       // echo $form->textFieldGroup($model,'username'); 
        ?>
       
	<?php echo $form->passwordFieldGroup($model,'password',array(
        'hint'=>' ',
    )); ?>
       
	<?php echo $form->checkBoxGroup($model,'rememberMe'); ?>
</fieldset>
	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
            'buttonType'=>'submit',
            'context'=>'primary',
            'label'=>'Вход',
        )); ?>
                 	<?php   $this->widget('booster.widgets.TbButton',array(
   'context'=>'primary',
   'label'=>'Регистрация',
   'buttonType' =>'link',
   'url'=>'index.php?r=site/registration'
)); ?>
            	<?php   $this->widget('booster.widgets.TbButton',array(
   'context'=>'primary',
   'label'=>'Вернуться на сайт',
   'buttonType' =>'link',
   'url'=>'../'
)); ?>
	</div>

<?php $this->endWidget(); ?>

</div>
</div>