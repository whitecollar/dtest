<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'[QiWi]-CRM Console Application',
        'timeZone' => 'Europe/Moscow',
	// preloading 'log' component
	'preload'=>array('log'),

	// application components
	'components'=>array(
		 'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=qiwi',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'M0u5Z8f1',
            'charset' => 'utf8',
            'enableProfiling'=>true,
            'schemaCachingDuration'=>3600,
            'enableParamLogging'=>true,
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, trace, profile, info',
				),
			),
		),
	),
);