<?php
 Yii::setPathOfAlias('booster', dirname(__FILE__).'/../extensions/booster');
 return array(
    
       'aliases' => array(
                'booster' => realpath(__DIR__ . '/../extensions/booster'), // Линк на бустер
         ),
       
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'test',
        //'language'=> 'ru',
	// preloading 'log' component
        // 'defaultController' => 'main/admin', 
	'preload'=>array('log','booster'),
    
        'timeZone' => 'Europe/Moscow',
	 
	'import'=>array(
               
		'application.models.*',
		'application.components.*',
             
                'booster.helpers.TbHtml',
                'booster.helpers.TbArray',
                
	),
        'controllerMap'=>array(
      
        
        ),
           'theme'=>'bootstrap',  

	'modules'=>array(
            
                 'gii'=>array(
                        'class' => 'system.gii.GiiModule',
                        'password'=>'123',
                        'ipFilters' => array('178.169.92.254','80.91.192.35','80.91.192.50','89.105.139.218', '::1'),
                        'generatorPaths'=>array(
                        'booster.gii',
                 ),
        ),
            
           
        ),

 
	'components'=>array(
   
     
                    'authManager' => array(
                 
                    'class' => 'PhpAuthManager',
                 
                    'defaultRoles' => array('guest'),
                ),
         
            
      
         'booster' => array(
                  'class' => 'booster.components.Booster',
                  'responsiveCss'=>'true',
               
                  'minify'=>'true',
              
                  'bootstrapCss'=>'true',
                  'responsiveCss'=>'true',
                  'fontAwesomeCss'=>'true',
                  'yiiCss'=>'true',
                  'enableJS'=>'true',
                  'enableBootboxJS'=>'true',
                  'enableNotifierJS'=>'true',
                  'ajaxCssLoad'=>'true',
                  'ajaxJsLoad'=>'true',
                  'coreCss' => true,
            
        ),
            
		
	 
         'user'=>array(
                   'class' => 'WebUser',
                
                    'allowAutoLogin'=>true,
        ),
            
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=dtest',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'lexroot',
            'charset' => 'utf8',
            'enableProfiling'=>true,
            'schemaCachingDuration'=>3600,
            'enableParamLogging'=>true,
        ),
            
		
        'errorHandler'=>array(
	        'errorAction'=>'site/error',
        ),
            
            
	'log'=>array(
	'class'=>'CLogRouter',
	'enabled'=>YII_DEBUG,
	'routes'=>array(
                    
		array(
			'class'=>'CFileLogRoute',
			 'levels'=>'error, warning, trace, profile, info',
		),
		 
	),
),
	),

	 
	'params'=>array(
	 
		'adminEmail'=>'admin@kiwi-crm.com',
	),
);