<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
            $dataProvider=OcProduct::model()->findByPk(Yii::app()->user->getId());
                $dataProviderOC=OcProduct::model()->findByPk(Yii::app()->user->getId());
                $dataProviderU=User::model()->findByPk(Yii::app()->user->getId());
                $dataProviderDs=OcProductDescription::model()->findByPk(Yii::app()->user->getId());
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
                        'dataProvideroc'=>$dataProviderOC,
                        'dataProviderU'=>$dataProviderU,
                        'dataProviderDs'=>$dataProviderDs,
		));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

      
        
          public function actionRegistration()
     {
        // тут думаю все понятно
       $model=new LoginForm;
       $form = new User();
        $formOc = new OcProduct();
        // Проверяем являеться ли пользователь гостем
        // ведь если он уже зарегистрирован - формы он не должен увидеть.
        if (!Yii::app()->user->isGuest) {
             throw new CException('Вы уже зарегистрированны!');
        } else {
            // Если $_POST['User'] не пустой массив - значит была отправлена форма
            // следовательно нам надо заполнить $form этими данными
             // и провести валидацию. Если валидация пройдет успешно - пользователь
            // будет зарегистрирован, не успешно - покажем ошибку на экран
            if (!empty($_POST['User'])) {
                
                 // Заполняем $form данными которые пришли с формы
                $form->attributes = $_POST['User'];
               // $formOc->attributes = $_POST['OcProduct'];
                //$formSt->attributes = $_POST['OcProductToStore'];
               //$formDs->attributes = $_POST['OcProductDescription'];
                // Запоминаем данные которые пользователь ввёл в капче
                
                
                    // В validate мы передаем название сценария. Оно нам может понадобиться
                    // когда будем заниматься созданием правил валидации [читайте дальше]
                     if($form->validate('registration')) {
                        // Если валидация прошла успешно...
                        // Тогда проверяем свободен ли указанный логин..

                            if ($form->model()->count("phone = :login", array(':login' => $form->phone))) {
                                 // Указанный логин уже занят. Создаем ошибку и передаем в форму
                                $form->addError('phone', 'Данный номер уже зарегистрирован');
                                $this->render("registration", array('form' => $form, 'formOc' => $formOc));
                             } else {
                                // Выводим страницу что "все окей"
                                $form->save();
        
                                //Вот тут залепон , заполняем атрибуты для нового пользователя 
        $atr1= OcAttribute::model()->findAll();
        $user= User::model()->find('phone=:product_id', array(':product_id'=>$form->phone));
        foreach ($atr1 as $at1) {
      //  echo $at1['attribute_id'];
            //Вот тут какой то адовый подьебос с заполнением атрибутов надо разобраться
       
       $sql ='INSERT INTO oc_product_attribute ( product_id , attribute_id, language_id, text)
 VALUES ('.$user['id'].', '.$at1['attribute_id'].', 1, "-") ON DUPLICATE KEY UPDATE text="-";';
         $connection=Yii::app()->db;                     
      $command=$connection->createCommand($sql);
      $rowCount=$command->execute();
       //  print_r($user['id']);
        }
                               //    $formOc->save();
                                $this->redirect("index.php?r=site/login" ,array('model'=>$model));
                            }
                                             
                    } else {
                        // Если введенные данные противоречат 
                        // правилам валидации (указаны в rules) тогда
                        // выводим форму и ошибки.
                         // [Внимание!] Нам ненадо передавать ошибку в отображение,
                        // Она автоматически после валидации цепляеться за 
                        // $form и будет [автоматически] показана на странице с 
                         // формой! Так что мы тут делаем простой рэндер.
                        
                        $this->render("registration", array( 'form' => $form, 'formOc' => $formOc ));
                    }
             } else {
                // Если $_POST['User'] пустой массив - значит форму некто не отправлял.
                // Это значит что пользователь просто вошел на страницу регистрации
                // и ему мы должны просто показать форму.
                 
                 $this->render("registration", array('form' => $form, 'formOc' => $formOc));
            }
        }
    }
	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}